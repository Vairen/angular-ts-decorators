import * as tslib_1 from "tslib";
import * as angular from 'angular';
import { camelToKebab, Declaration, defineMetadata, getAttributeName, getMetadata, getTypeDeclaration, getTypeName, isAttributeSelector, kebabToCamel, metadataKeys } from './utils';
import { ElementRef } from './element_ref';
import { ngLifecycleHooksMap } from './lifecycle_hooks';
export function Component(_a) {
    var selector = _a.selector, options = tslib_1.__rest(_a, ["selector"]);
    return function (ctrl) {
        options.controller = ctrl;
        var isAttrSelector = isAttributeSelector(selector);
        var bindings = getMetadata(metadataKeys.bindings, ctrl);
        if (bindings) {
            if (isAttrSelector) {
                options['bindToController'] = bindings;
                options['controllerAs'] = options['controllerAs'] || '$ctrl';
            }
            else
                options['bindings'] = bindings;
        }
        var require = getMetadata(metadataKeys.require, ctrl);
        if (require) {
            options.require = require;
        }
        if (isAttrSelector) {
            options.restrict = 'A';
        }
        replaceLifecycleHooks(ctrl);
        var selectorName = isAttrSelector ? getAttributeName(selector) : selector;
        defineMetadata(metadataKeys.name, kebabToCamel(selectorName), ctrl);
        defineMetadata(metadataKeys.declaration, isAttrSelector ? Declaration.Directive : Declaration.Component, ctrl);
        defineMetadata(metadataKeys.options, options, ctrl);
    };
}
/** @internal */
export function registerComponent(module, component) {
    var name = getMetadata(metadataKeys.name, component);
    var options = getMetadata(metadataKeys.options, component);
    var listeners = getMetadata(metadataKeys.listeners, options.controller);
    var viewChildren = getMetadata(metadataKeys.viewChildren, component);
    if (listeners || viewChildren) {
        options.controller = extendWithHostListenersAndChildren(options.controller, listeners, viewChildren);
    }
    options.controller = appendElementRef(options.controller);
    module.component(name, options);
}
/** @internal */
export function extendWithHostListenersAndChildren(ctrl, listeners, viewChildren) {
    if (listeners === void 0) { listeners = {}; }
    if (viewChildren === void 0) { viewChildren = {}; }
    var handlers = Object.keys(listeners);
    var properties = Object.keys(viewChildren);
    var createdListeners = [];
    var NewCtrl = /** @class */ (function (_super) {
        tslib_1.__extends(NewCtrl, _super);
        function NewCtrl($element, $scope) {
            var args = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                args[_i - 2] = arguments[_i];
            }
            var _this = _super.apply(this, args) || this;
            _this.$element = $element;
            _this.$scope = $scope;
            return _this;
        }
        NewCtrl.prototype._updateViewChildren = function () {
            var _this = this;
            properties.forEach(function (property) {
                var child = viewChildren[property];
                var selector;
                if (typeof child.selector !== 'string') {
                    var type = getTypeDeclaration(child.selector);
                    if (type !== Declaration.Component && type !== Declaration.Directive) {
                        console.error("No valid selector was provided for ViewChild" + (child.first ? '' :
                            'ren') + " decorator, it should be type or selector of component/directive");
                        return;
                    }
                    selector = camelToKebab(getTypeName(child.selector));
                    var parseViews_1 = function (element) {
                        var el = angular.element(element);
                        return child.read ? new ElementRef(el) : el.controller(kebabToCamel(typeof child.selector === 'string' ? element.localName : selector));
                    };
                    if (child.first) {
                        var element = _this.$element[0].querySelector(selector);
                        var views = parseViews_1(element);
                        views && (_this[property] = views);
                    }
                    else {
                        var viewChildEls = Array.prototype.slice.call(_this.$element[0].querySelectorAll(selector))
                            .map(function (viewChild) { return parseViews_1(viewChild); })
                            .filter(function (el) { return !!el; });
                        viewChildEls.length && (_this[property] = viewChildEls);
                    }
                }
                else {
                    selector = "[__" + child.selector + "]";
                    var parseViews_2 = function (element) {
                        if (element) {
                            var el = angular.element(element);
                            return new ElementRef(el);
                        }
                    };
                    if (child.first) {
                        var element = _this.$element[0].querySelector(selector);
                        _this[property] = parseViews_2(element);
                    }
                    else {
                        _this[property] = _this.$element[0].querySelectorAll(selector)
                            .map(function (element) { return parseViews_2(element); });
                    }
                }
            });
        };
        NewCtrl.prototype.$postLink = function () {
            var _this = this;
            handlers.forEach(function (handler) {
                var eventName = listeners[handler].eventName;
                var bind = function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    _this[handler].apply(_this, args);
                    _this.$scope.$applyAsync();
                };
                _this.$element.on(eventName, bind);
                createdListeners.push(function () { return _this.$element.off(eventName, bind); });
            });
            this._updateViewChildren();
            if (_super.prototype.$postLink) {
                _super.prototype.$postLink.call(this);
            }
        };
        NewCtrl.prototype.$onChanges = function (changes) {
            if (_super.prototype.$onChanges) {
                _super.prototype.$onChanges.call(this, changes);
            }
            this._updateViewChildren();
        };
        NewCtrl.prototype.$onDestroy = function () {
            if (_super.prototype.$onDestroy) {
                _super.prototype.$onDestroy.call(this);
            }
            if (createdListeners.length) {
                createdListeners.forEach(function (listener) { return listener(); });
            }
        };
        return NewCtrl;
    }(ctrl));
    NewCtrl.$inject = ['$element', '$scope'].concat(ctrl.$inject || []);
    return NewCtrl;
}
export function appendElementRef(ctrl) {
    if (ctrl.$inject) {
        var elemRef_1 = ctrl.$inject
            .reduce(function (replaceIndexes, injectable, index, injections) {
            if (injectable == 'ElementRef') {
                replaceIndexes.push(index);
                injections[index] = '$element';
            }
            return replaceIndexes;
        }, []);
        if (elemRef_1.length) {
            var NewCtrl = /** @class */ (function (_super) {
                tslib_1.__extends(NewCtrl, _super);
                function NewCtrl() {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    var _this = this;
                    elemRef_1
                        .forEach(function (index) {
                        args[index] = new ElementRef(args[index]);
                    });
                    _this = _super.apply(this, args) || this;
                    return _this;
                }
                return NewCtrl;
            }(ctrl));
            return NewCtrl;
        }
    }
    return ctrl;
}
/** @internal */
export function replaceLifecycleHooks(ctrl) {
    var ctrlClass = ctrl.prototype;
    var ngHooksFound = getHooksOnCtrlClass(ctrlClass);
    ngHooksFound.forEach(function (ngHook) {
        var angularJsHook = ngLifecycleHooksMap[ngHook];
        ctrlClass[angularJsHook] = ctrlClass[ngHook];
    });
}
/** @internal */
function getHooksOnCtrlClass(ctrlClass) {
    return Object.keys(ngLifecycleHooksMap)
        .filter(function (hook) { return angular.isFunction(ctrlClass[hook]); });
}
//# sourceMappingURL=component.js.map