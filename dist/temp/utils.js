import 'reflect-metadata';
export var Declaration;
(function (Declaration) {
    Declaration["Component"] = "Component";
    Declaration["Directive"] = "Directive";
    Declaration["Pipe"] = "Pipe";
})(Declaration || (Declaration = {}));
/** @internal */
export var metadataKeys = {
    declaration: 'custom:declaration',
    name: 'custom:name',
    bindings: 'custom:bindings',
    require: 'custom:require',
    options: 'custom:options',
    listeners: 'custom:listeners',
    viewChildren: 'custom:viewChildren',
};
export function kebabToCamel(input) {
    return input.replace(/(-\w)/g, function (m) { return m[1].toUpperCase(); });
}
export function camelToKebab(str) {
    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}
/** @internal */
export function getAttributeName(selector) {
    return selector.substr(1, selector.length - 2);
}
/** @internal */
export function isAttributeSelector(selector) {
    return /^[\[].*[\]]$/g.test(selector);
}
/** @internal */
export function getMetadata(metadataKey, target) {
    return Reflect.getMetadata(metadataKey, target);
}
/** @internal */
export function defineMetadata(metadataKey, metadataValue, target) {
    Reflect.defineMetadata(metadataKey, metadataValue, target);
}
export function getTypeName(target) {
    return getMetadata(metadataKeys.name, target);
}
export function getTypeDeclaration(target) {
    return getMetadata(metadataKeys.declaration, target);
}
//# sourceMappingURL=utils.js.map