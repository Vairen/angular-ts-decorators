import { defineMetadata, getMetadata, metadataKeys } from './utils';
export function Input(alias) {
    return function (target, key) { return addBindingToMetadata(target, key, '<?', alias); };
}
export function Output(alias) {
    return function (target, key) { return addBindingToMetadata(target, key, '&', alias); };
}
export function ViewParent(controller) {
    return function (target, key) { return addRequireToMetadata(target, key, controller); };
}
/** @internal */
function addBindingToMetadata(target, key, direction, alias) {
    var targetConstructor = target.constructor;
    var bindings = getMetadata(metadataKeys.bindings, targetConstructor) || {};
    bindings[key] = alias || direction;
    defineMetadata(metadataKeys.bindings, bindings, targetConstructor);
}
/** @internal */
function addRequireToMetadata(target, key, controller) {
    var targetConstructor = target.constructor;
    var require = getMetadata(metadataKeys.require, targetConstructor) || {};
    require[key] = controller;
    defineMetadata(metadataKeys.require, require, targetConstructor);
}
//# sourceMappingURL=input.js.map