import * as angular from 'angular';
import { registerPipe } from './pipe';
import { registerProviders } from './injectable';
import { Declaration, getMetadata, metadataKeys } from './utils';
import { registerComponent } from './component';
import { registerDirective } from './directive';
export function NgModule(_a) {
    var id = _a.id, _b = _a.bootstrap, bootstrap = _b === void 0 ? [] : _b, _c = _a.declarations, declarations = _c === void 0 ? [] : _c, _d = _a.imports, imports = _d === void 0 ? [] : _d, _e = _a.providers, providers = _e === void 0 ? [] : _e;
    return function (Class) {
        // module registration
        var deps = imports.map(function (mod) {
            if (typeof mod === 'function' && mod['module']) {
                return mod['module'].name;
            }
            return mod;
        });
        if (!id) {
            id = Class.name || /\s*function\s+([^(\s]*)\s*/.exec(Class.toString())[1];
        }
        var module = angular.module(id, deps);
        // components, directives and filters registration
        declarations.forEach(function (declaration) {
            var declarationType = getMetadata(metadataKeys.declaration, declaration);
            switch (declarationType) {
                case Declaration.Component:
                    registerComponent(module, declaration);
                    break;
                case Declaration.Directive:
                    registerDirective(module, declaration);
                    break;
                case Declaration.Pipe:
                    registerPipe(module, declaration);
                    break;
                default:
                    console.error("Can't find type metadata on " + declaration.name + " declaration, did you forget to decorate it?\n            Decorate your declarations using @Component, @Directive or @Pipe decorator.");
            }
        });
        // services registration
        if (providers) {
            registerProviders(module, providers);
        }
        // Class constructor as run service
        module.run((Class['$inject'] || []).concat(function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return new ((_a = Function.prototype.bind).call.apply(_a, [Class, null].concat(args)));
        }));
        // config and run blocks registration
        var config = Class.config, run = Class.run;
        if (config) {
            module.config(config);
        }
        if (run) {
            module.run(run);
        }
        // expose angular module as static property
        Class.module = module;
        // expose name attribute
        Object.defineProperty(Class, 'name', { value: id });
    };
}
//# sourceMappingURL=module.js.map