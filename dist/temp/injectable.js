import { defineMetadata, getMetadata, metadataKeys } from './utils';
export function Injectable(name) {
    return function (Class) {
        if (!name) {
            name = Class.name || /\s*function\s+([^(\s]*)\s*/.exec(Class.toString())[1];
        }
        defineMetadata(metadataKeys.name, name, Class);
    };
}
export function Inject(name) {
    return function (target, propertyKey, parameterIndex) {
        // if @Inject decorator is on target's method
        if (propertyKey && Array.isArray(target[propertyKey])) {
            target[propertyKey][parameterIndex] = name;
            return; // exit, don't change injection on target's constructor
        }
        // if @Inject decorator is on target's constructor
        if (!target.$inject) {
            target.$inject = [];
        }
        target.$inject[parameterIndex] = name;
    };
}
/** @internal */
export function registerProviders(module, providers) {
    var multi = {};
    providers.forEach(function (provider, index) {
        // providers registered using { provide, useClass/useFactory/useValue } syntax
        if (provider.provide) {
            var name_1 = provider.provide;
            // Parse Multiple
            if (provider.multi) {
                name_1 = name_1 + index;
                multi[provider.provide] = multi[provider.provide] || [];
                multi[provider.provide].push(name_1);
            }
            if (provider.useClass && provider.useClass instanceof Function) {
                module.service(name_1, provider.useClass);
            }
            else if (provider.useFactory && provider.useFactory instanceof Function) {
                provider.useFactory.$inject = provider.deps || provider.useFactory.$inject;
                module.factory(name_1, provider.useFactory);
            }
            else if (provider.useValue) {
                module.constant(name_1, provider.useValue);
            }
        }
        // providers registered as classes
        else {
            var name_2 = getMetadata(metadataKeys.name, provider);
            if (!name_2) {
                console.error(provider.name + " was not registered as angular service:\n        Provide explicit name in @Injectable when using class syntax or register it using object provider syntax:\n        { provide: '" + provider.name + "', useClass: " + provider.name + " }");
            }
            else {
                module.service(name_2, provider);
            }
        }
    });
    Object.keys(multi)
        .forEach(function (multiName) {
        module.factory(multiName, multi[multiName].concat(function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return args;
        }));
    });
}
//# sourceMappingURL=injectable.js.map