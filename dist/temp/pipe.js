import { Declaration, defineMetadata, getMetadata, metadataKeys } from './utils';
export function Pipe(options) {
    return function (Class) {
        defineMetadata(metadataKeys.name, options.name, Class);
        defineMetadata(metadataKeys.options, options.pure, Class);
        defineMetadata(metadataKeys.declaration, Declaration.Pipe, Class);
    };
}
/** @internal */
export function registerPipe(module, filter) {
    var name = getMetadata(metadataKeys.name, filter);
    var isPure = getMetadata(metadataKeys.options, filter);
    var filterFactory = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var injector = args[0]; // reference to $injector
        var instance = injector.instantiate(filter);
        var transform = instance.transform.bind(instance);
        transform.$stateful = isPure === undefined ? false : !isPure;
        return transform;
    };
    filterFactory.$inject = ['$injector'].concat(filter.$inject || []);
    module.filter(name, filterFactory);
    module.service(name, filterFactory);
}
//# sourceMappingURL=pipe.js.map