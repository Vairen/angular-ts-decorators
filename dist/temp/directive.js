import * as tslib_1 from "tslib";
import { Declaration, defineMetadata, getAttributeName, getMetadata, isAttributeSelector, kebabToCamel, metadataKeys } from './utils';
import { extendWithHostListenersAndChildren, replaceLifecycleHooks, appendElementRef } from './component';
export function Directive(_a) {
    var selector = _a.selector, options = tslib_1.__rest(_a, ["selector"]);
    return function (ctrl) {
        var bindings = getMetadata(metadataKeys.bindings, ctrl);
        if (bindings) {
            options.bindToController = bindings;
        }
        var require = getMetadata(metadataKeys.require, ctrl);
        if (require) {
            options.require = require;
        }
        options.restrict = options.restrict || 'A';
        var selectorName = isAttributeSelector(selector) ? getAttributeName(selector) : selector;
        defineMetadata(metadataKeys.name, kebabToCamel(selectorName), ctrl);
        defineMetadata(metadataKeys.declaration, Declaration.Directive, ctrl);
        defineMetadata(metadataKeys.options, options, ctrl);
    };
}
/** @internal */
export function registerDirective(module, ctrl) {
    var directiveFunc;
    var name = getMetadata(metadataKeys.name, ctrl);
    var options = getMetadata(metadataKeys.options, ctrl);
    replaceLifecycleHooks(ctrl);
    var listeners = getMetadata(metadataKeys.listeners, ctrl);
    var viewChildren = getMetadata(metadataKeys.viewChildren, ctrl);
    options.controller = listeners || viewChildren ?
        extendWithHostListenersAndChildren(ctrl, listeners, viewChildren) : ctrl;
    options.controller = appendElementRef(options.controller);
    directiveFunc = function () { return options; };
    module.directive(name, directiveFunc);
}
//# sourceMappingURL=directive.js.map