/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * A wrapper around a native element inside of a View.
 * @stable
 */
var ElementRef = /** @class */ (function () {
    function ElementRef($element) {
        $element['nativeElement'] = $element[0];
        return $element;
    }
    return ElementRef;
}());
export { ElementRef };
//# sourceMappingURL=element_ref.js.map