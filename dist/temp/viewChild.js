import { defineMetadata, getMetadata, metadataKeys } from './utils';
export function ViewChild(selector, opts) {
    if (opts === void 0) { opts = {}; }
    return function (target, key) { return addBindingToMetadata(target, key, selector, opts.read, true); };
}
export function ViewChildren(selector, opts) {
    if (opts === void 0) { opts = {}; }
    return function (target, key) { return addBindingToMetadata(target, key, selector, opts.read, false); };
}
/** @internal */
function addBindingToMetadata(target, key, selector, read, first) {
    var targetConstructor = target.constructor;
    var viewChildren = getMetadata(metadataKeys.viewChildren, targetConstructor) || {};
    viewChildren[key] = { first: first, selector: selector, read: read };
    defineMetadata(metadataKeys.viewChildren, viewChildren, targetConstructor);
}
//# sourceMappingURL=viewChild.js.map