import { bootstrap, element } from 'angular';
export var platformBrowserDynamic = function () { return PlatformRef; };
var PlatformRef = /** @class */ (function () {
    function PlatformRef() {
    }
    PlatformRef.bootstrapModule = function (moduleType, compilerOptions) {
        if (compilerOptions === void 0) { compilerOptions = { strictDi: false }; }
        var moduleName;
        switch (typeof moduleType) {
            case 'string': // module name string
                moduleName = moduleType;
                break;
            case 'object': // angular.module object
                moduleName = moduleType.name;
                break;
            case 'function': // NgModule class
            default:
                var module = moduleType.module;
                if (!module) {
                    throw Error('Argument moduleType should be NgModule class, angular.module object or module name string');
                }
                moduleName = module.name;
        }
        var strictDi = (compilerOptions.strictDi === true);
        element(document).ready(function () {
            bootstrap(document.body, [moduleName], { strictDi: strictDi });
        });
    };
    return PlatformRef;
}());
export { PlatformRef };
//# sourceMappingURL=bootstrap.js.map