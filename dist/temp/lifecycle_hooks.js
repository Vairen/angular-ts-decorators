/**
 * @internal
 * @desc Mapping between angular and angularjs LifecycleHooks
 */
export var ngLifecycleHooksMap = {
    ngOnInit: '$onInit',
    ngOnDestroy: '$onDestroy',
    ngDoCheck: '$doCheck',
    ngOnChanges: '$onChanges',
    ngAfterViewInit: '$postLink'
};
//# sourceMappingURL=lifecycle_hooks.js.map