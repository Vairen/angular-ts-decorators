import { defineMetadata, getMetadata, metadataKeys } from './utils';
export function HostListener(eventName, args) {
    return function (target, propertyKey, descriptor) {
        var listener = descriptor.value;
        if (typeof listener !== 'function') {
            throw new Error("@HostListener decorator can only be applied to methods not: " + typeof listener);
        }
        var targetConstructor = target.constructor;
        /**
         * listeners = { onMouseEnter: { eventName: 'mouseenter mouseover', args: [] } }
         */
        var listeners = getMetadata(metadataKeys.listeners, targetConstructor) || {};
        listeners[propertyKey] = { eventName: eventName, args: args };
        defineMetadata(metadataKeys.listeners, listeners, targetConstructor);
    };
}
//# sourceMappingURL=hostListener.js.map