import { IComponentController, IModule } from 'angular';
import { Type } from './type';
import { Provider } from './provider';
export interface ModuleConfig {
    id?: string;
    declarations?: Array<Type<any> | any[]>;
    imports?: Array<Type<any> | NgModule | any[] | string>;
    exports?: Function[];
    providers?: Provider[];
    bootstrap?: IComponentController[];
}
export interface NgModule {
    module?: IModule;
    config?(...args: any[]): any;
    run?(...args: any[]): any;
    [p: string]: any;
}
export declare function NgModule({ id, bootstrap, declarations, imports, providers }: ModuleConfig): (Class: NgModule) => void;
