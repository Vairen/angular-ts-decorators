import * as angular from 'angular';
export interface ComponentOptionsDecorated extends angular.IComponentOptions {
    selector: string;
    styles?: any[];
    restrict?: string;
    replace?: boolean;
}
export declare function Component({ selector, ...options }: ComponentOptionsDecorated): (ctrl: angular.IControllerConstructor) => void;
export declare function appendElementRef(ctrl: any): any;
