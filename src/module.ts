import * as angular from 'angular';
import { IComponentController, IDirectiveFactory, IModule, Injectable } from 'angular';

import { Type } from './type';
import { PipeTransform, registerPipe } from './pipe';
import { registerProviders } from './injectable';
import { Declaration, getMetadata,  metadataKeys } from './utils';
import { registerComponent } from './component';
import { registerDirective } from './directive';
import { Provider } from './provider';

export interface ModuleConfig {
  id?: string;
  declarations?: Array<Type<any> | any[]>; // https://angular.io/api/core/NgModule
  imports?: Array<Type<any> | NgModule | any[] | string>; // https://angular.io/api/core/NgModule
  exports?: Function[];
  providers?: Provider[];
  bootstrap?: IComponentController[];
}

export interface NgModule {
  module?: IModule;
  config?(...args: any[]): any;
  run?(...args: any[]): any;
  [p: string]: any;
}

export function NgModule({ id, bootstrap = [], declarations = [], imports = [], providers = [] }: ModuleConfig) {
  return (Class: NgModule) => {
    // module registration
    const deps = imports.map((mod) => {
      if (typeof mod === 'function' && mod['module']) {
        return mod['module'].name;
      }
      return mod;
    });
    if (!id) {
      id = (Class as any).name || /\s*function\s+([^(\s]*)\s*/.exec(Class.toString())[1];
    }
    const module = angular.module(id, deps);

    // components, directives and filters registration
    declarations.forEach((declaration: any) => {
      const declarationType = getMetadata(metadataKeys.declaration, declaration);
      switch (declarationType) {
        case Declaration.Component:
          registerComponent(module, declaration);
          break;
        case Declaration.Directive:
          registerDirective(module, declaration);
          break;
        case Declaration.Pipe:
          registerPipe(module, declaration);
          break;
        default:
          console.error(
            `Can't find type metadata on ${declaration.name} declaration, did you forget to decorate it?
            Decorate your declarations using @Component, @Directive or @Pipe decorator.`
          );
      }
    });

    // services registration
    if (providers) {
      registerProviders(module, providers);
    }

    // Class constructor as run service
    module.run((Class['$inject'] || []).concat((...args: any[]) => {
      return new (Function.prototype.bind.call(Class, null, ...args));
    }));

    // config and run blocks registration
    const { config, run } = Class;
    if (config) {
      module.config(config);
    }
    if (run) {
      module.run(run);
    }

    // expose angular module as static property
    Class.module = module;
    // expose name attribute
    Object.defineProperty(Class, 'name', { value: id });
  };
}
