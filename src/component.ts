import * as angular from 'angular';

import {
    camelToKebab,
    Declaration,
    defineMetadata,
    getAttributeName,
    getMetadata,
    getTypeDeclaration,
    getTypeName,
    isAttributeSelector,
    kebabToCamel,
    metadataKeys
} from './utils';
import { ElementRef } from './element_ref';
import { IHostListeners } from './hostListener';
import { IViewChildren } from './viewChild';
import { ngLifecycleHooksMap } from './lifecycle_hooks';

export interface ComponentOptionsDecorated extends angular.IComponentOptions {
    selector: string;
    styles?: any[];
    restrict?: string;
    replace?: boolean;
}

export function Component({ selector, ...options }: ComponentOptionsDecorated) {
    return (ctrl: angular.IControllerConstructor) => {
        options.controller = ctrl;
        const isAttrSelector = isAttributeSelector(selector);
        const bindings = getMetadata(metadataKeys.bindings, ctrl);
        if (bindings) {
            if (isAttrSelector) {
                options['bindToController'] = bindings;
                options['controllerAs'] = options['controllerAs'] || '$ctrl';
            }
            else options['bindings'] = bindings;
        }

        const require = getMetadata(metadataKeys.require, ctrl);
        if (require) {
            options.require = require;
        }

        if (isAttrSelector) {
            (options as angular.IDirective).restrict = 'A';
        }

        replaceLifecycleHooks(ctrl);

        const selectorName = isAttrSelector ? getAttributeName(selector) : selector;
        defineMetadata(metadataKeys.name, kebabToCamel(selectorName), ctrl);
        defineMetadata(metadataKeys.declaration, isAttrSelector ? Declaration.Directive : Declaration.Component, ctrl);
        defineMetadata(metadataKeys.options, options, ctrl);
    };
}

/** @internal */
export function registerComponent(module: angular.IModule, component: angular.IComponentController) {
    const name = getMetadata(metadataKeys.name, component);
    const options = getMetadata(metadataKeys.options, component);
    const listeners: IHostListeners = getMetadata(metadataKeys.listeners, options.controller);
    const viewChildren: IViewChildren = getMetadata(metadataKeys.viewChildren, component);
    if (listeners || viewChildren) {
        options.controller = extendWithHostListenersAndChildren(options.controller, listeners, viewChildren);
    }
    options.controller = appendElementRef(options.controller);
    module.component(name, options);
}

/** @internal */
export function extendWithHostListenersAndChildren(ctrl: { new(...args: any[]) },
                                                   listeners: IHostListeners = {},
                                                   viewChildren: IViewChildren = {}) {
    const handlers = Object.keys(listeners);
    const properties = Object.keys(viewChildren);
    const createdListeners = [];

    class NewCtrl extends ctrl {
        public constructor(
            private $element: angular.IAugmentedJQuery,
            private $scope: angular.IScope,
            ...args: any[]
        ){
            super(...args);
        }

        private _updateViewChildren() {
            properties.forEach((property) => {
                const child = viewChildren[property];
                let selector: string;

                if (typeof child.selector !== 'string') {
                    const type = getTypeDeclaration(child.selector);
                    if (type !== Declaration.Component && type !== Declaration.Directive) {
                        console.error(`No valid selector was provided for ViewChild${child.first ? '' :
                            'ren'} decorator, it should be type or selector of component/directive`);
                        return;
                    }
                    selector = camelToKebab(getTypeName(child.selector));

                    let parseViews = function (element) {
                        const el: angular.IAugmentedJQuery = angular.element(element);
                        return child.read ? new ElementRef(el) : el.controller(kebabToCamel(typeof child.selector === 'string' ? element.localName : selector));
                    };

                    if (child.first) {
                        const element: HTMLElement = this.$element[0].querySelector(selector);
                        let views = parseViews(element);
                        views && (this[property] = views);
                    } else {
                        const viewChildEls = Array.prototype.slice.call(this.$element[0].querySelectorAll(selector))
                            .map((viewChild: HTMLElement) => parseViews(viewChild))
                            .filter(el => !!el);
                        viewChildEls.length && (this[property] = viewChildEls);
                    }
                } else {
                    selector = `[__${child.selector}]`;

                    let parseViews = function (element) {
                        if (element) {
                            const el: angular.IAugmentedJQuery = angular.element(element);
                            return new ElementRef(el);
                        }
                    };

                    if (child.first) {
                        let element = this.$element[0].querySelector(selector);
                        this[property] = parseViews(element);
                    } else {
                        this[property] = this.$element[0].querySelectorAll(selector)
                            .map((element) => parseViews(element));
                    }
                }
            });
        }

        public $postLink() {
            handlers.forEach((handler) => {
                const { eventName } = listeners[handler];
                let bind = (...args: any[]) => {
                    this[handler].apply(this, args);
                    this.$scope.$applyAsync();
                };
                this.$element.on(eventName, bind);
                createdListeners.push(() => this.$element.off(eventName, bind));
            });
            this._updateViewChildren();
            if (super.$postLink) {
                super.$postLink();
            }
        }

        public $onChanges(changes) {
            if (super.$onChanges) {
                super.$onChanges(changes);
            }
            this._updateViewChildren();
        }

        public $onDestroy() {
            if (super.$onDestroy) {
                super.$onDestroy();
            }
            if (createdListeners.length) {
                createdListeners.forEach((listener: Function) => listener());
            }
        }
    }

    NewCtrl.$inject = ['$element', '$scope', ...ctrl.$inject || []];
    return NewCtrl;
}

export function appendElementRef(ctrl) {
    if (ctrl.$inject) {
        let elemRef = ctrl.$inject
            .reduce((replaceIndexes: any[], injectable: string, index: number, injections: any[]) => {
                if (injectable == 'ElementRef') {
                    replaceIndexes.push(index);
                    injections[index] = '$element';
                }
                return replaceIndexes;
            }, []);

        if (elemRef.length) {
            class NewCtrl extends ctrl {
                public constructor(...args: any[]) {
                    elemRef
                        .forEach((index: number) => {
                            args[index] = new ElementRef(args[index]);
                        });
                    super(...args);
                }
            }
            return NewCtrl;
        }
    }
    return ctrl;
}

/** @internal */
export function replaceLifecycleHooks(ctrl: angular.IControllerConstructor) {
    const ctrlClass = ctrl.prototype;
    const ngHooksFound = getHooksOnCtrlClass(ctrlClass);

    ngHooksFound.forEach((ngHook: string) => {
        const angularJsHook: string = ngLifecycleHooksMap[ngHook];
        ctrlClass[angularJsHook] = ctrlClass[ngHook];
    });
}

/** @internal */
function getHooksOnCtrlClass(ctrlClass: any): string[] {
    return Object.keys(ngLifecycleHooksMap)
        .filter((hook: string) => angular.isFunction(ctrlClass[hook]));
}
