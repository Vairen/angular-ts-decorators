import { Declaration, defineMetadata, getMetadata, metadataKeys } from './utils';
import { IModule } from 'angular';

export interface PipeTransformConstructor {
  new(...args: any[]): PipeTransform;
}

export interface PipeTransform {
  transform(...args: any[]): any;
}

export function Pipe(options: {name: string, pure?: boolean}) {
  return (Class: PipeTransformConstructor) => {
    defineMetadata(metadataKeys.name, options.name, Class);
    defineMetadata(metadataKeys.options, options.pure, Class);
    defineMetadata(metadataKeys.declaration, Declaration.Pipe, Class);
  };
}

/** @internal */
export function registerPipe(module: IModule, filter: PipeTransformConstructor) {
  const name = getMetadata(metadataKeys.name, filter);
  const isPure = getMetadata(metadataKeys.options, filter);
  const filterFactory = (...args: any[]) => {
    const injector = args[0]; // reference to $injector
    const instance = injector.instantiate(filter);
    const transform = instance.transform.bind(instance);
    transform.$stateful = isPure === undefined ? false : !isPure;
    return transform;
  };
  filterFactory.$inject = ['$injector', ...filter.$inject || []];
  module.filter(name, filterFactory);
  module.service(name, filterFactory);
}
